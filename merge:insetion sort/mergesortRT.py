# Stephanie Ayala
# mergesort - Average input runtimes: averaging five runtimes with inputs of different sizes
#!/usr/bin/python
import time

# mergesort function
def mergesort(data):
    # if the list is greater than 1 then it needs to be sorted
    if len(data)>1:
        #initialize counters
        i, j, k = 0, 0, 0
        # find the middledle of the list
        middle = len(data)//2
        # split the data into two halves
        low = data[:middle]
        high = data[middle:]
        # recursivly sort both halves
        mergesort(low)
        mergesort(high)
        # while there is more than one element in each recursive list
        while i < len(low) and j < len(high):
            # sort the numbers and incriment the counters
            if low[i] < high[j]:
                data[k]=low[i]
                i += 1
            else: # if high is greater than or equal to low
                data[k]=high[j]
                j += 1
            k=k+1
        # copy low and high elements and incriment
        while i < len(low):
            data[k]=low[i]
            i +=1
            k +=1
        while j < len(high):
            data[k]=high[j]
            j +=1
            k +=1

# check each runtime five times
for j in range(0,5):
    import random
    # create list
    data = []
    # get random numbers in the range of 0 to 10000
    for i in range(0,10000):
        # get 10000 random numbers
        x = random.randint(1,10000)
        # add number to the list
        data.append(x)
    # get time before mergesort
    t0 = time.clock()
    mergesort(data)
    # get time after mergesort
    t1 = time.clock()
    # get the time difference
    t3 = t1 - t0
    total = 0
    # get the total time
    total += t3
# get the average of these times
average1 = total / 5

# do this for the rest of the different sizes of inputs
for j in range(0,5):
    import random
    data = []
    for i in range(0,20000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average2 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,30000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average3 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,40000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average4 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,50000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average5 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,60000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average6 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,70000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average7 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,80000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average8 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,90000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average9 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,100000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    mergesort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average10 = total / 5

# input runtimes into file
with open('mergeRunTime.out', "w"):
    pass
with open('mergeRunTime.out', 'a') as fileout:
    fileout.write("n = 10,000\n")
    fileout.write("Run Time = %f\n" % average1)
    fileout.write("n = 20,000\n")
    fileout.write("Run Time = %f\n" % average2)
    fileout.write("n = 30,000\n")
    fileout.write("Run Time = %f\n" % average3)
    fileout.write("n = 40,000\n")
    fileout.write("Run Time = %f\n" % average4)
    fileout.write("n = 50,000\n")
    fileout.write("Run Time = %f\n" % average5)
    fileout.write("n = 60,000\n")
    fileout.write("Run Time = %f\n" % average6)
    fileout.write("n = 70,000\n")
    fileout.write("Run Time = %f\n" % average7)
    fileout.write("n = 80,000\n")
    fileout.write("Run Time = %f\n" % average8)
    fileout.write("n = 90,000\n")
    fileout.write("Run Time = %f\n" % average9)
    fileout.write("n = 100,000\n")
    fileout.write("Run Time = %f\n" % average10)



# https://www.tutorialspoint.com/python/time_clock.htm
# https://www.geeksforgeeks.org/merge-sort/
# https://www.tutorialspoint.com/python/python_files_io.htm
# https://pythonprogramminglanguage.com/randon-numbers/
