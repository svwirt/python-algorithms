# Stephanie Ayala
# insertsort - file input/output
#!/usr/bin/python

def insertsort(arr):
    size = len(arr) - 1
    # dont use first value in list
    data = arr[1:]
    # go through each value
    for i in range(size):
        # set value to keyvalue
        keyval = data[i]
        # keep track of position
        pos = i
        # compare the keyvalue and switch if needed
        while pos > 0 and data[pos-1] > keyval:
            data[pos] = data[pos-1]
            pos = pos-1
            data[pos] = keyval
    # write numbers to file out
    with open('insert.out', 'a') as fileout:
        for d in data:
            fileout.write("%i " % d)
        fileout.write("\n")

# create list
data = []
# get list from data file
with open("data.txt") as file:
    # go through each line in the file
    for line in file:
        # split the numbers
        line = line.split()
        if line:
            # add numbers through list
            data.append([int(i) for i in line])
# make sure out file is clear
with open('insert.out', "w"):
    pass
# call insertsort for eavh list in the list
for i in range(len(data)):
    insertsort(data[i])




# https://www.geeksforgeeks.org/insertion-sort/
# https://www.tutorialspoint.com/python/python_files_io.htm
