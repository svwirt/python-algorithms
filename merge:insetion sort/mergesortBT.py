# Stephanie Ayala
# mergesort - Best case inputs: inputs of different sizes that are already sorted
#!/usr/bin/python
import time

# mergesort function
def mergesort(data):
    # if the list is greater than 1 then it needs to be sorted
    if len(data)>1:
        #initialize counters
        i, j, k = 0, 0, 0
        # find the middledle of the list
        middle = len(data)//2
        # split the data into two halves
        low = data[:middle]
        high = data[middle:]
        # recursivly sort both halves
        mergesort(low)
        mergesort(high)
        # while there is more than one element in each recursive list
        while i < len(low) and j < len(high):
            # sort the numbers and incriment the counters
            if low[i] < high[j]:
                data[k]=low[i]
                i += 1
            else: # if high is greater than or equal to low
                data[k]=high[j]
                j += 1
            k=k+1
        # copy low and high elements and incriment
        while i < len(low):
            data[k]=low[i]
            i +=1
            k +=1
        while j < len(high):
            data[k]=high[j]
            j +=1
            k +=1
# create array
data = []
# start with 0 and incriment up
data_value = 0
# input of 2000 numbers
data_max = 2000
while data_value <= data_max:
    # add value to array
    data.append(data_value)
    # incriment value
    data_value += 1 
# start time
t0 = time.clock()
# run mergesort on values
mergesort(data)
# get process time 
t1 = time.clock()
# time after mergesort minus time before mergesort
t3 = t1 - t0
time1 = t3

# repeat with different values
data = []
data_value = 0
data_max = 4000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time2 = t3

data = []
data_value = 0
data_max = 8000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time3 = t3

data = []
data_value = 0
data_max = 16000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time4 = t3

data = []
data_value = 0
data_max = 32000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time5 = t3

data = []
data_value = 0
data_max = 64000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time6 = t3

data = []
data_value = 0
data_max = 128000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time7 = t3

data = []
data_value = 0
data_max = 256000
while data_value <= data_max:
    data.append(data_value)
    data_value += 1  
t0 = time.clock()
mergesort(data)
t1 = time.clock()
t3 = t1 - t0
time8 = t3

# input times and numbers into output file
with open('mergesortBT.out', "w"):
    pass
with open('mergesortBT.out', 'a') as fileout:
    fileout.write("n = 2,000\n")
    fileout.write("Run Time = %f\n" % time1)
    fileout.write("n = 4,000\n")
    fileout.write("Run Time = %f\n" % time2)
    fileout.write("n = 8,000\n")
    fileout.write("Run Time = %f\n" % time3)
    fileout.write("n = 16,000\n")
    fileout.write("Run Time = %f\n" % time4)
    fileout.write("n = 32,000\n")
    fileout.write("Run Time = %f\n" % time5)
    fileout.write("n = 64,000\n")
    fileout.write("Run Time = %f\n" % time6)
    fileout.write("n = 128,000\n")
    fileout.write("Run Time = %f\n" % time7)
    fileout.write("n = 256,000\n")
    fileout.write("Run Time = %f\n" % time8)


# https://www.tutorialspoint.com/python/time_clock.htm
# https://www.geeksforgeeks.org/merge-sort/
# https://www.tutorialspoint.com/python/python_files_io.htm
