# Stephanie Ayala
# insertsort - Average input runtimes: averaging five runtimes with inputs of different sizes
#!/usr/bin/python
import time

# insertion sort function
def insertsort(data):
    size = len(data) 
    # go through each value
    for i in range(size):
        # set value to keyvalue
        keyval = data[i]
        # keep track of position
        pos = i
        # compare the keyvalue and switch if needed
        while pos > 0 and data[pos-1] > keyval:
            data[pos] = data[pos-1]
            pos = pos-1
            data[pos] = keyval

# run each data size five times
for j in range(0,5):
    import random
    data = []
    # get 1000 values
    for i in range(0,1000):
        # get random value from 1 to 10000
        x = random.randint(1,10000)
        # add value to list
        data.append(x)
    # get time before sort
    t0 = time.clock()
    insertsort(data)
    # get time after sort
    t1 = time.clock()
    # get time difference
    t3 = t1 - t0
    total = 0
    # get total of all five times
    total += t3
# get average of these times
average1 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,2000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average2 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,3000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average3 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,4000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average4 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,5000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average5 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,6000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average6 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,7000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average7 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,8000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average8 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,9000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average9 = total / 5

for j in range(0,5):
    import random
    data = []
    for i in range(0,10000):
        x = random.randint(1,10000)
        data.append(x)
    t0 = time.clock()
    insertsort(data)
    t1 = time.clock()
    t3 = t1 - t0
    total = 0
    total += t3
average10 = total / 5


# write run times to out file
with open('insertRunTime.out', "w"):
    pass
with open('insertRunTime.out', 'a') as fileout:
    fileout.write("n = 1,000\n")
    fileout.write("Run Time = %f\n" % average1)
    fileout.write("n = 2,000\n")
    fileout.write("Run Time = %f\n" % average2)
    fileout.write("n = 3,000\n")
    fileout.write("Run Time = %f\n" % average3)
    fileout.write("n = 4,000\n")
    fileout.write("Run Time = %f\n" % average4)
    fileout.write("n = 5,000\n")
    fileout.write("Run Time = %f\n" % average5)
    fileout.write("n = 6,000\n")
    fileout.write("Run Time = %f\n" % average6)
    fileout.write("n = 7,000\n")
    fileout.write("Run Time = %f\n" % average7)
    fileout.write("n = 8,000\n")
    fileout.write("Run Time = %f\n" % average8)
    fileout.write("n = 9,000\n")
    fileout.write("Run Time = %f\n" % average9)
    fileout.write("n = 10,000\n")
    fileout.write("Run Time = %f\n" % average10)

# https://www.tutorialspoint.com/python/time_clock.htm
# https://www.geeksforgeeks.org/insertion-sort/
# https://www.tutorialspoint.com/python/python_files_io.htm