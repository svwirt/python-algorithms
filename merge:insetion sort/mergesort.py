# Stephanie Ayala
# mergesort - file input/output

#!/usr/bin/python

# mergesort function
def mergesort(data):
	# if the list is greater than 1 then it needs to be sorted
    if len(data)>1:
        #initialize counters
        i, j, k = 0, 0, 0
    	# find the middledle of the list
        middle = len(data)//2
        # split the data into two halves
        low = data[:middle]
        high = data[middle:]
        # recursivly sort both halves
        mergesort(low)
        mergesort(high)
        # while there is more than one element in each recursive list
        while i < len(low) and j < len(high):
            # sort the numbers and incriment the counters
            if low[i] < high[j]:
                data[k]=low[i]
                i += 1
            else: # if high is greater than or equal to low
                data[k]=high[j]
                j += 1
            k=k+1
        # copy low and high elements and incriment
        while i < len(low):
            data[k]=low[i]
            i +=1
            k +=1
        while j < len(high):
            data[k]=high[j]
            j +=1
            k +=1

# create list
data = []
# read numbers into list, make a list of lists for each row
with open("data.txt") as file:
    for line in file:
        line = line.split()
        if line:
            data.append([int(i) for i in line])
# clear out file if it already exists
with open('merge.out', "w"):
    pass
# for each element of the list (of lists)
for i in range(len(data)):
    arr = data[i]
    # dont use the first number
    arr = arr[1:]
    mergesort(arr)
    # write numbers to file
    with open('merge.out', 'a') as fileout:
        for d in arr:
            fileout.write("%i " % d)
        fileout.write("\n")

# https://www.geeksforgeeks.org/merge-sort/
# https://www.tutorialspoint.com/python/python_files_io.htm
# https://www.tutorialspoint.com/python/time_clock.htm