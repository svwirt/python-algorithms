# Stephanie Ayala
# insertsort - Worst case input runtimes: lists in reverse order

#!/usr/bin/python
import time

# insertion sort function
def insertsort(data):
    size = len(data) 
    # go through each value
    for i in range(size):
        # set value to keyvalue
        keyval = data[i]
        # keep track of position
        pos = i
        # compare the keyvalue and switch if needed
        while pos > 0 and data[pos-1] > keyval:
            data[pos] = data[pos-1]
            pos = pos-1
            data[pos] = keyval

data = []
data_value = 1000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time1 = t3

data = []
data_value = 2000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time2 = t3

data = []
data_value = 3000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time3 = t3

data = []
data_value = 4000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time4 = t3

data = []
data_value = 5000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time5 = t3

data = []
data_value = 6000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time6 = t3

data = []
data_value = 7000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time7 = t3

data = []
data_value = 8000
while data_value != 0:
    data.append(data_value)
    data_value -= 1  
t0 = time.clock()
insertsort(data)
t1 = time.clock()
t3 = t1 - t0
time8 = t3

with open('insertsortWT.out', "w"):
    pass
with open('insertsortWT.out', 'a') as fileout:
    fileout.write("n = 1,000\n")
    fileout.write("Run Time = %f\n" % time1)
    fileout.write("n = 2,000\n")
    fileout.write("Run Time = %f\n" % time2)
    fileout.write("n = 3,000\n")
    fileout.write("Run Time = %f\n" % time3)
    fileout.write("n = 4,000\n")
    fileout.write("Run Time = %f\n" % time4)
    fileout.write("n = 5,000\n")
    fileout.write("Run Time = %f\n" % time5)
    fileout.write("n = 6,000\n")
    fileout.write("Run Time = %f\n" % time6)
    fileout.write("n = 7,000\n")
    fileout.write("Run Time = %f\n" % time7)
    fileout.write("n = 8,000\n")
    fileout.write("Run Time = %f\n" % time8)



    # https://www.tutorialspoint.com/python/time_clock.htm
    # https://www.geeksforgeeks.org/insertion-sort/