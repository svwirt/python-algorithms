import string, linecache, re

def knapSack(W, wt, val, n, F): 
    # initalize the table with 0s
    Table = [[0 for w in range(W + 1)] for i in range(n + 1)] 
    # initialize list to hold items used
    items = []          
    # build table (bottom up)
    for i in range(n + 1): 
        for w in range(W + 1): 
            # base case
            if i == 0 or w == 0: 
                Table[i][w] = 0
            # prior value
            # new option = value of current item + value of remaining
            # table position = the max of prior value and new option
            elif wt[i - 1] <= w: 
                Table[i][w] = max(val[i - 1]  
                  + Table[i - 1][w - wt[i - 1]], 
                               Table[i - 1][w]) 
            # take the table value from the row immediately above it
            else: 
                Table[i][w] = Table[i - 1][w] 
    # table is stored
    total = Table[n][W] 
    priceReturn = total
    w = W 
    for i in range(n, 0, -1): 
        if total == Table[i - 1][w]: 
            continue
        # if the total is not equal to the table index
        #         then we subtract the value from the total
        #         and subtract the weight from the total weight
        #         and add item to the items array
        else: 
            items.append(i)
            total = total - val[i - 1] 
            w = w - wt[i - 1] 
    return priceReturn, items



def shoppingSpree(P, W, M, N, F):
        # initialize variables
        total_price = 0
        gt = 0
        lis = []
        # call the knapSack function for each member of the family
        for i in range(F):
            # get tupple returned from knapsack with price and items
            price, li = knapSack(M[i], W, P, N, F)
            # add up total price for family
            total_price = total_price + int(price)
            # the list of items is backwards
            li.reverse()
            # make a list of each items list
            lis.append(li)
        # write total for family to file
        with open('shopping.out', 'a') as fileout:
            fileout.write("\nTotal Price ")
            fileout.write(str(total_price))
            fileout.write("\n")
            # write members items for each member of the family
            fileout.write("Member Items")
        # for each list of items in larger list
        for i in range(len(lis)):
            with open('shopping.out', 'a') as fileout:
                fileout.write("\n")
                fileout.write(str(i + 1))
                fileout.write(str(": "))
                # for each item in list write to file
                cat = lis[i]
                # print(cat)
                # print(len(cat))
                # print("\n")
                catLen = len(cat)
                for k in range(catLen):
                    if(catLen == 0):
                        continue
                    else:
                        print
                        fileout.write(str(cat[k]))
                        fileout.write(" ")

            # for each item in list write to file
            # while(count < len(lis[i]) - 1):
            #     with open('shopping.out', 'a') as fileout:
            #             fileout.write(str(li[j]))
            #             fileout.write(" ")
        with open('shopping.out', 'a') as fileout:
            fileout.write("\n")

filename = "shopping.txt"
# read a selected line (first line is 1)
# note that each line will have a trailing '\n'
# get the test case number

T = int(linecache.getline(filename, 1)) 
# make sure shopping.out is empty
with open('shopping.out', "w"):
    pass 
index = 2
#for each test case
for i in range(T):
    # write test case to file
    with open('shopping.out', 'a') as fileout:
        fileout.write("Test Case %i " % (i + 1))
    # initialize arrays for price, weight, max weight
    P = []
    W = []
    M = []
    # get the number of items
    # index = index + l
    N = int(linecache.getline(filename, index))
    # print("N=",N)
    a = 3 + N
    # for each item
    index +=1
    for j in range(N):
        # read line to get price/weight
        PW = linecache.getline(filename, index + j)
        # print("PW=", PW)
        PW = re.split('\W+',PW)
        Price = int(PW[0])
        # build an array of prices for this test case
        P.append(Price)
        if(PW[1] == ""):
            Weight = 0
        # build an array of weights for this test case
        else:
            Weight = int(PW[1])
        W.append(Weight)
    index = index + N
    # get the number of family members
    F = int(linecache.getline(filename, index))
    index += 1
    for k in range(F):
        # get the max weight for each family member - build an array
        Max = int(linecache.getline(filename, index  + k))
        M.append(Max)

       
    index = index + F

    # call shopping spree
    shoppingSpree(P, W, M, N, F)



# https://www.geeksforgeeks.org/0-1-knapsack-problem-dp-10/
# https://www.daniweb.com/programming/software-development/threads/196512/how-do-i-read-a-specfic-line-from-a-file
# https://docs.python.org/3/library/re.html
